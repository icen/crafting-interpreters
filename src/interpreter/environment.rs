use std::{cell::RefCell, collections::HashMap, rc::Rc};

use super::value::Value;

#[derive(PartialEq, Debug, Clone)]
pub struct Environment(Rc<RefCell<EnvInner>>);

#[derive(PartialEq, Debug, Clone)]
struct EnvInner {
    local: HashMap<String, Value>,
    parent: Option<Environment>
}

impl Environment {
    pub fn new() -> Environment {
        Environment(Rc::new(RefCell::new(EnvInner {
            local: HashMap::new(),
            parent: None
        })))
    }

    pub fn push(parent: Environment) -> Environment {
        Environment(Rc::new(RefCell::new(EnvInner {
            local: HashMap::new(),
            parent: Some(parent),
        })))
    }

    pub fn parent(&self) -> Option<Environment> {
        match &(*self.0).borrow().parent {
            Some(p) => Some(p.clone()),
            None => None
        }
    }

    pub fn lookup(&self, name: &str) -> Option<Value> {
        (*self.0).borrow().local
            .get(name).cloned()
            .or(if let Some(p) = self.parent() {
                p.lookup(name)
            } else {
                None
            })
    }

    pub fn lookup_at(&self, name: &str, depth: usize) -> Option<Value> {
	let mut env = self.clone();
	for _ in 0..depth {
	    env = env.parent()?;
	}
	let x = (*env.0).borrow().local.get(name).cloned();
	x
    }

    pub fn amend_at(&mut self, name: &str, value: Value, depth: usize) {
	let mut env = self.clone();
	for _ in 0..depth {
	    env = env.parent().expect("ICE: name resolution past root scope");
	}
	(*env.0).borrow_mut().local.insert(name.to_owned(), value);
    }

    pub fn define(&mut self, name: String, value: Value) {
        assert_eq!(None, self.0.borrow_mut().local.insert(name, value));
    }
}
