use std::{collections::HashMap, fmt::{self, Formatter}};

use crate::{syntax::Statement, tokens::Token};

use super::environment::Environment;

#[derive(PartialEq, Debug, Clone)]
pub enum Value {
    Nil,
    Bool(bool),
    Number(f64),
    String(String),
    Function(Function),
    Native { name: &'static str, function: fn() -> Value },
    Class(Class),
    Instance { class: Class, state: Environment },
}

#[derive(PartialEq, Debug, Clone)]
pub struct Function {
    pub args: Vec<Token>,
    pub body: Vec<Statement>,
    pub env: Environment
}

#[derive(PartialEq, Debug, Clone)]
pub struct Class {
    pub name: String,
    pub methods: HashMap<String, Function>,
}

impl Value {
    pub fn describe(&self) -> &'static str {
        match *self {
            Value::Nil => "nil",
            Value::Bool(_) => "boolean",
            Value::Number(_) => "number",
            Value::String(_) => "string",
            Value::Function { .. } => "function",
            Value::Native { .. } => "native",
	    Value::Class { .. } => "class",
	    Value::Instance { .. } => "instance",
        }
    }

    pub fn truthy(&self) -> bool {
        match *self {
            Value::Nil | Value::Bool(false) => false,
            _ => true
        }
    }
}

impl fmt::Display for Value {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Value::Nil => write!(f, "nil"),
            Value::Bool(b) => write!(f, "{}", b),
            Value::Number(n) => write!(f, "{}", n),
            Value::String(s) => write!(f, "{}", s),
            Value::Function(Function { args, ..}) => {
                write!(f, "function(")?;
                for (i, arg) in args.iter().enumerate() {
                    write!(f, "{}{}", if i > 0 { "," } else { "" }, arg.name("").unwrap())?;
                }
                write!(f, ")")
            }
            Value::Native { name, .. } => write!(f, "<native {}>", name),
	    Value::Class(c) => write!(f, "{}", c),
	    Value::Instance { class, .. } => write!(f, "instance of {}", class),
        }
    }
}

impl fmt::Display for Class {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
	write!(f, "class {}", self.name)
    }
} 
