use crate::{Error, tokens::{Token, TokenValue}};
use std::str::FromStr;

static PUNCTUATION: [char; 7] = ['(', ')', '{', '}', ',', '.', ';'];
static OPERATORS: [&'static str; 12] = ["+", "-", "*", "/", "!=", "!", "==", "=", ">=", ">", "<", "<="];
static KEYWORDS: [&'static str; 16] = ["and", "or", "nil", "class", "fun", "super", "this", "var",
				      "true", "false", "if", "else", "return", "print", "for", "while"];

pub fn lex(full_src: &str, filename: &str) -> Result<Vec<Token>, Error> {
    fn number(src: &str) -> Option<Result<(TokenValue, &str, &str), String>> {
        let (lexeme, rest) = match src.find(|c: char| !c.is_numeric()) {
            Some(0) => return None,
            Some(head) => {
                if src[head..].starts_with('.') {
                    match src[head + 1..].find(|c: char| !c.is_numeric()) {
                        Some(tail) => src.split_at(head + 1 + tail),
                        None => (src, ""),
                    }
                } else {
                    src.split_at(head)
                }
            }
            None => (src, "")
        };

        match f64::from_str(lexeme) {
            Ok(float) => Some(Ok((TokenValue::Number(float), lexeme, rest))),
            Err(e) => Some(Err(format!("{}", e)))
        }
    }

    fn string(src: &str) -> Option<Result<(TokenValue, &str, &str), String>> {
        if !src.starts_with('"') {
            return None;
        }

        let mut iter = src.chars().enumerate().skip(1);
        let mut skip = false;
        let mut value = String::new();
        loop {
            match iter.next() {
                None => return Some(Err("Unclosed string literal".to_string())),
                Some((_, c)) if skip => {
                    skip = false;
                    // push on escaped characters
                    match c {
                        'n' => value.push('\n'),
                        'r' => value.push('\r'),
                        't' => value.push('\t'),
                        c => value.push(c),
                    }
                }
                Some((_, '\\')) => skip = true,
                Some((i, '"')) => {
                    let (head, rest) = src.split_at(i + 1);
                    return Some(Ok((TokenValue::String(value), head, rest)));
                }
                Some((_, c)) => value.push(c),
            }
        }
    }

    fn identifier(src: &str) -> Option<Result<(TokenValue, &str, &str), String>> {
        if !src.starts_with(char::is_alphabetic) {
            // identifiers must start with a letter
            return None;
        }

        // but then can contain any alphanumeric string, with underscores
        match src.find(|c: char| !c.is_alphanumeric() || c == '_').unwrap_or(src.len()) {
            0 => None,
            n => {
                let (lexeme, rest) = src.split_at(n);
                Some(Ok((TokenValue::Identifier(lexeme.to_owned()), lexeme, rest)))
            }
        }
    }

    fn punctuation(src: &str) -> Option<Result<(TokenValue, &str, &str), String>> {
	for c in &PUNCTUATION {
            if src.starts_with(*c) {
		return Some(Ok((TokenValue::Punctuation(*c), &src[..c.len_utf8()], &src[c.len_utf8()..])));
            }
	}

        None
    }

    fn operator<'a>(src: &'a str) -> Option<Result<(TokenValue, &'a str, &'a str), String>> {
	for op in &OPERATORS {
            if src.starts_with(op) {
		return Some(Ok((TokenValue::Operator(op), op, &src[op.len()..])));
            }
	}

        None
    }

    fn keyword<'a>(src: &'a str) -> Option<Result<(TokenValue, &'a str, &'a str), String>> {
	for word in &KEYWORDS {
	    if src.starts_with(word) && !src[word.len()..].starts_with(|c: char| c.is_alphanumeric() || c == '_') {
		return Some(Ok((TokenValue::Keyword(word), word, &src[word.len()..])));
	    }
	}
	None
    }


    fn comment<'a>(start: &'static str, end: &'static str, src: &'a str) -> Option<Result<(TokenValue, &'a str, &'a str), String>> {
        if src.starts_with(start) {
            let end_of_line = src.find(end).unwrap_or(src.len());
            let (head, tail) = src.split_at(end_of_line);
            return Some(Ok((TokenValue::Comment, head, tail)));
        }

        None
    }

    fn whitespace(src: &str) -> Option<Result<(TokenValue, &str, &str), String>> {
        match src.find(|c: char| !c.is_whitespace()).unwrap_or(src.len()) {
            0 => None,
            n => {
                let (head, tail) = src.split_at(n);
                Some(Ok((TokenValue::Whitespace, head, tail)))
            }
        }
    }

    let mut src = full_src;
    let mut tokens = Vec::new();
    let mut line = 0;
    let mut column = 0;
    let mut index = 0;

    while !src.is_empty() {
        let (token_type, lexeme, rest) = match
        comment("//", "\n", src)
            .or_else(|| comment("/*", "*/", src))
            .or_else(|| whitespace(src))
            .or_else(|| punctuation(src))
            .or_else(|| operator(src))
            .or_else(|| string(src))
            .or_else(|| number(src))
            .or_else(|| keyword(src))
            .or_else(|| identifier(src)) {
            Some(Ok(x)) => x,
            None => return Err(Error {
                message: format!("Unrecognised character: {}", src.chars().next().expect("ICE: src not expected to be empty")),
                file: filename.to_owned(),
                line,
                column,
            }),
            Some(Err(e)) => return Err(Error {
                message: e,
                file: filename.to_owned(),
                line,
                column,
            })
        };


        let (lines, cols) = lexeme.split('\n')
            .enumerate()
            .map(|(i, c)| (i, c.len()))
            .last()
            .expect("Lexeme cannot be empty");
        let advance = lexeme.len();

        if let TokenValue::Comment | TokenValue::Whitespace = token_type {
            // we don't care about these
        } else {
            tokens.push(Token {
                value: token_type,
                line,
                column,
                index,
            });
        }

        if lines == 0 {
            column += cols;
        } else {
            line += lines;
            column = cols;
        }
        index += advance;

        src = rest;
    }

    tokens.push(Token {
        value: TokenValue::EOF,
        line,
        column,
        index,
    });

    Ok(tokens)
}

#[test]
fn lex_groupers() {
    match lex("(( )){} // grouping stuff", "test") {
        Ok(tokens) => tokens,
        Err(e) => panic!("{}", e.message)
    };
}

#[test]
fn lex_error_on_unrecognised() {
    match lex("a + %b", "test") {
        Ok(_) => panic!(),
        Err(_) => ()
    }
}

#[test]
fn lex_allows_unrecognised_in_string() {
    match lex("\"this is not valid%%%&lox source\"", "test") {
        Ok(_) => (),
        Err(_) => panic!()
    }
}
