use crate::Error;
use crate::tokens::{Token, TokenType, TokenValue};
use crate::syntax::*;

pub fn parse(tokens: &[Token], filename: &str) -> Result<Program, Error> {
    struct Parser<'a> {
        filename: &'a str,
        tokens: &'a [Token],
        index: usize,
    }

    impl<'a> Parser<'a> {
        fn new(filename: &'a str, tokens: &'a [Token]) -> Parser<'a> {
            Parser { filename, tokens, index: 0 }
        }

        /// Advance the cursor and return the consumed token
        fn advance(&mut self) -> &Token {
            if self.type_at() != TokenType::EOF {
                self.index += 1;
            }

            &self.tokens[self.index - 1]
        }

        fn at(&self) -> &Token {
            &self.tokens[self.index]
        }

        fn type_at(&self) -> TokenType {
            self.at().get_type()
        }

        fn previous(&self) -> &Token {
            &self.tokens[self.index - 1]
        }

        fn expect(&mut self, value: TokenValue) -> Result<(), Error> {
            if self.at().value == value {
                self.advance();
                Ok(())
            } else {
                Err(self.error(format!("Expected {:?}, found {:?}", value, self.at().value), self.at()))
            }
        }

        /// Bails the current set of tokens, resetting on the start of the next statement
        fn bail(&mut self, message: &str) -> Error {
            let cause = self.at().clone();
            self.advance();
            while self.type_at() != TokenType::EOF {
                if self.previous().value == TokenValue::Punctuation(';') {
                    break;
                }

                match self.at().value {
                    TokenValue::Keyword("class") |
                    TokenValue::Keyword("fun") |
                    TokenValue::Keyword("for") |
                    TokenValue::Keyword("if") |
                    TokenValue::Keyword("while") |
                    TokenValue::Keyword("print") |
                    TokenValue::Keyword("return") => break,
                    _ => self.advance()
                };
            }

            self.error(format!("Expected {}, found {:?}", message, cause.value), &cause)
        }

        fn error(&self, message: String, cause: &Token) -> Error {
            cause.error_at(message, self.filename.to_owned())
        }
    }

    fn program(parser: &mut Parser) -> Result<Program, Error> {
        let mut statements = Vec::new();
        while parser.at().get_type() != TokenType::EOF {
            statements.push(statement(parser)?);
        }

        Ok(statements)
    }

    fn variable(parser: &mut Parser) -> Result<Statement, Error> {
        match parser.advance() {
            Token { value: TokenValue::Keyword("var"), .. } => (),
            _ => {
                return Err(parser.bail("keyword 'var'"));
            }
        }

        let name = match parser.advance() {
            token @ Token { value: TokenValue::Identifier(_), .. } => token.clone(),
            _ => {
                return Err(parser.bail("identifier"));
            }
        };

        let initialiser = if let TokenValue::Operator("=") = parser.at().value {
            parser.advance();
            Some(expression(parser)?)
        } else {
            None
        };

        match parser.advance().clone() {
            Token { value: TokenValue::Punctuation(';'), .. } => {
                Ok(Statement::Var { name, initialiser })
            }
            t => Err(parser.error(format!("Expected semicolon, found {:?}", t.value), &t))
        }
    }

    fn function_declaration(parser: &mut Parser) -> Result<Statement, Error> {
        parser.expect(TokenValue::Keyword("fun"))?;
        function(parser)
    }

    fn identifier(parser: &mut Parser) -> Result<Token, Error> {
        match parser.advance() {
            token @ Token { value: TokenValue::Identifier(_), .. } => Ok(token.clone()),
            t => {
                let msg = format!("Expected identifier, found: {:?}", t.get_type());
                let cause = t.clone();
                return Err(parser.error(msg, &cause));
            }
        }
    }

    fn function(parser: &mut Parser) -> Result<Statement, Error> {
        let name = identifier(parser)?;
        parser.expect(TokenValue::Punctuation('('))?;
        let mut args = vec!();
        if parser.at().value != TokenValue::Punctuation(')') {
            args.push(identifier(parser)?);
            while parser.at().value == TokenValue::Punctuation(',') {
                parser.expect(TokenValue::Punctuation(','))?;
                args.push(identifier(parser)?);
            }
        }
        parser.expect(TokenValue::Punctuation(')'))?;
        let body = block(parser)?;

        Ok(Statement::Function { name, args, body })
    }

    fn statement(parser: &mut Parser) -> Result<Statement, Error> {
        let start = parser.at().clone();
        match &start.value {
	    TokenValue::Keyword("class") => class(parser),
            TokenValue::Keyword("var") => variable(parser),
            TokenValue::Keyword("fun") => function_declaration(parser),
            TokenValue::Keyword("print") => {
                parser.advance();
                let statement = Statement::Print(expression(parser)?);
                parser.expect(TokenValue::Punctuation(';'))
                    .and(Ok(statement))
            }
            TokenValue::Keyword("if") => {
                parser.advance();
                parser.expect(TokenValue::Punctuation('('))?;
                let predicate = expression(parser)?;
                parser.expect(TokenValue::Punctuation(')'))?;
                let branch = statement(parser)?;

                if parser.at().value == TokenValue::Keyword("else") {
                    parser.advance();
                    let alternative = statement(parser)?;
                    Ok(Statement::If(predicate,
                                     Box::new(branch),
                                     Some(Box::new(alternative))))
                } else {
                    Ok(Statement::If(predicate, Box::new(branch), None))
                }
            }
            TokenValue::Keyword("while") => {
                parser.advance();
                parser.expect(TokenValue::Punctuation('('))?;
                let predicate = expression(parser)?;
                parser.expect(TokenValue::Punctuation(')'))?;
                let body = statement(parser)?;
                Ok(Statement::While(Some(predicate), Box::new(body)))
            }
            TokenValue::Keyword("for") => for_loop(parser),
            TokenValue::Keyword("return") => {
                // Expression here is optional, and if missing, desugar to 'nil'
                parser.advance();
                let expr = if let TokenValue::Punctuation(';') = parser.at().value {
                        Expr::Literal {
                            token: Token {
                                value: TokenValue::Keyword("nil"),
                                line: parser.at().line,
                                column: parser.at().column,
                                index: parser.at().index,
                            }
                        }
                } else {
                    expression(parser)?
                };

                parser.expect(TokenValue::Punctuation(';'))?;
                Ok(Statement::Return(expr))
            }
            TokenValue::Punctuation('{') => Ok(Statement::Block(block(parser)?)),
            _ => {
                let statement = Statement::Expr(expression(parser)?);
                parser.expect(TokenValue::Punctuation(';')).and(Ok(statement))
            }
        }
    }

    fn class(parser: &mut Parser) -> Result<Statement, Error> {
	let site = parser.advance().clone();
	let name = identifier(parser)?;
	let mut methods = Vec::new();
	parser.expect(TokenValue::Punctuation('{'))?;
	while parser.at().value != TokenValue::Punctuation('}') && parser.at().value != TokenValue::EOF {
	    methods.push(function(parser)?);
	}
	parser.expect(TokenValue::Punctuation('}'))?;
	Ok(Statement::Class { name, methods, site })
    }

    fn for_loop(parser: &mut Parser) -> Result<Statement, Error> {
	parser.advance();
        parser.expect(TokenValue::Punctuation('('))?;
        let prelude = if parser.at().value == TokenValue::Punctuation(';') {
            parser.advance();
            None
        } else {
            Some(statement(parser)?)
        };
        let condition = if parser.at().value == TokenValue::Punctuation(';') {
            parser.advance();
            None
        } else {
            let cond = Some(expression(parser)?);
            parser.expect(TokenValue::Punctuation(';'))?;
            cond
        };
        let postlude = if parser.at().value == TokenValue::Punctuation(')') {
            None
        } else {
            Some(expression(parser)?)
        };
        parser.expect(TokenValue::Punctuation(')'))?;
        let body = statement(parser)?;

        match (prelude, postlude) {
            (Some(prelude), Some(postlude)) => {
                Ok(Statement::Block(vec!(
                    prelude,
                    Statement::While(condition, Box::new(Statement::Block(
                        vec!(body, Statement::Expr(postlude))))))))
            }
            (None, Some(postlude)) => {
                Ok(Statement::While(condition, Box::new(Statement::Block(vec!(
                    body, Statement::Expr(postlude))))))
            }
            (Some(prelude), None) => {
                Ok(Statement::Block(vec!(
                    prelude,
                    Statement::While(condition, Box::new(body)))))
            }
            (None, None) => Ok(Statement::While(condition, Box::new(body)))
        }

    }

    fn block(parser: &mut Parser) -> Result<Vec<Statement>, Error> {
        parser.advance();
        let mut declarations = Vec::new();
        while parser.at().value != TokenValue::Punctuation('}') && parser.at().value != TokenValue::EOF {
            declarations.push(statement(parser)?);
        }
        parser.expect(TokenValue::Punctuation('}'))?;
        Ok(declarations)
    }

    fn expression(parser: &mut Parser) -> Result<Expr, Error> {
        assignment(parser)
    }

    fn assignment(parser: &mut Parser) -> Result<Expr, Error> {
        let left = or(parser)?;
        if parser.at().value == TokenValue::Operator("=") {
            let equals = parser.advance().clone();
            let value = assignment(parser)?;

            match left {
                Expr::Variable { token } => Ok(Expr::Assignment { name: token, value: Box::new(value) }),
                _ => Err(parser.error(format!("Invalid assignment target"), &equals))
            }
        } else {
            Ok(left)
        }
    }

    fn or(parser: &mut Parser) -> Result<Expr, Error> {
        logical_operator(parser, and, &[TokenValue::Keyword("or")])
    }

    fn and(parser: &mut Parser) -> Result<Expr, Error> {
        logical_operator(parser, equality, &[TokenValue::Keyword("and")])
    }

    fn equality(parser: &mut Parser) -> Result<Expr, Error> {
        binary_operator(parser, comparison, &[TokenValue::Operator("=="), TokenValue::Operator("!=")])
    }

    fn comparison(parser: &mut Parser) -> Result<Expr, Error> {
        binary_operator(parser, term,
                        &[TokenValue::Operator(">"), TokenValue::Operator(">="),
                            TokenValue::Operator("<"), TokenValue::Operator("<=")])
    }

    fn term(parser: &mut Parser) -> Result<Expr, Error> {
        binary_operator(parser, factor, &[TokenValue::Operator("+"), TokenValue::Operator("-")])
    }

    fn factor(parser: &mut Parser) -> Result<Expr, Error> {
        binary_operator(parser, unary, &[TokenValue::Operator("/"), TokenValue::Operator("*")])
    }

    fn unary(parser: &mut Parser) -> Result<Expr, Error> {
        if parser.at().value == TokenValue::Operator("-") || parser.at().value == TokenValue::Operator("!") {
            let operator = parser.advance().clone();
            let rest = unary(parser)?;
            return Ok(Expr::Unary { operator, right: Box::new(rest) });
        }

        call(parser)
    }

    fn call(parser: &mut Parser) -> Result<Expr, Error> {
        let mut expr = primary(parser)?;
        while let TokenValue::Punctuation('(') = parser.at().value {
            parser.advance();
            expr = caller(expr, parser)?;
        }

        Ok(expr)
    }

    fn caller(expr: Expr, parser: &mut Parser) -> Result<Expr, Error> {
        let mut args = Vec::new();
        let site = parser.at().clone();
        if parser.at().value != TokenValue::Punctuation(')') {
            args.push(expression(parser)?);
            while parser.at().value == TokenValue::Punctuation(',') {
                parser.advance();
                args.push(expression(parser)?);
            }
        }

        parser.expect(TokenValue::Punctuation(')'))?;

        Ok(Expr::Call { function: Box::new(expr), args, site })
    }

    fn primary(parser: &mut Parser) -> Result<Expr, Error> {
        match parser.at().clone() {
            Token { value: TokenValue::Keyword("false"), .. } |
            Token { value: TokenValue::Keyword("true"), .. } |
            Token { value: TokenValue::Keyword("nil"), .. } |
            Token { value: TokenValue::String(_), .. } |
            Token { value: TokenValue::Number(_), .. } => Ok(Expr::Literal { token: parser.advance().clone() }),
            Token { value: TokenValue::Identifier(_), .. } => Ok(Expr::Variable { token: parser.advance().clone() }),
            Token { value: TokenValue::Punctuation('('), .. } => {
                let site = parser.advance().clone();
                let expr = expression(parser)?;
                match parser.at() {
                    Token { value: TokenValue::Punctuation(')'), .. } => {
                        parser.advance();
                        Ok(Expr::Group { expr: Box::new(expr), site })
                    }
                    t => Err(parser.error(format!("Unclosed parenthesis"), t))
                }
            }
            t => Err(parser.error(format!("Unexpected token: {:?}", t.value), &t))
        }
    }

    fn binary_operator<F: Fn(&mut Parser) -> Result<Expr, Error>>(parser: &mut Parser, surrounded: F, accepted: &[TokenValue]) -> Result<Expr, Error> {
        let mut expr = surrounded(parser)?;
        while accepted.iter().any(|value| *value == parser.at().value) {
            let operator = parser.advance().clone();
            let right = surrounded(parser)?;
            expr = Expr::Binary {
                left: Box::new(expr),
                operator,
                right: Box::new(right),
            };
        }

        Ok(expr)
    }

    fn logical_operator<F: Fn(&mut Parser) -> Result<Expr, Error>>(parser: &mut Parser, surrounded: F, accepted: &[TokenValue]) -> Result<Expr, Error> {
        let mut expr = surrounded(parser)?;
        while accepted.iter().any(|value| *value == parser.at().value) {
            let operator = parser.advance().clone();
            let right = surrounded(parser)?;
            expr = Expr::Logical {
                left: Box::new(expr),
                operator,
                right: Box::new(right),
            };
        }

        Ok(expr)
    }

    program(&mut Parser::new(filename, tokens))
}
