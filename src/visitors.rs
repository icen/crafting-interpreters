use crate::syntax::*;
use crate::tokens::Token;
use crate::Error;

pub trait ExprVisitor<T> {
    fn visit_expression(&mut self, expr: &Expr) -> Result<T, Error> {
        match expr {
            Expr::Binary { left, operator, right } => self.binary(left, operator, right),
            Expr::Logical { left, operator, right } => self.logical(left, operator, right),
            Expr::Group { site, expr } => self.group(expr, site),
            Expr::Literal { token } => self.literal(token),
            Expr::Unary { operator, right } => self.unary(operator, right),
            Expr::Variable { token } => self.variable(token),
            Expr::Assignment { name, value } => self.assignment(name, value),
            Expr::Call { site, function, args } => self.call(function, args, site),
        }
    }

    fn assignment(&mut self, identifier: &Token, value: &Expr) -> Result<T, Error>;
    fn binary(&mut self, w: &Expr, operator: &Token, x: &Expr) -> Result<T, Error>;
    fn call(&mut self, function: &Expr, args: &[Expr], site: &Token) -> Result<T, Error>;
    fn group(&mut self, expr: &Expr, site: &Token) -> Result<T, Error>;
    /// Resolution of a name
    fn variable(&mut self, identifier: &Token) -> Result<T, Error>;
    fn literal(&mut self, token: &Token) -> Result<T, Error>;
    fn unary(&mut self, operator: &Token, operand: &Expr) -> Result<T, Error>;
    /// This function must short-circuit the evaluation of `x`
    fn logical(&mut self, w: &Expr, operator: &Token, x: &Expr) -> Result<T, Error>;
}

pub trait StatementVisitor<T> {
    fn visit_statement(&mut self, statement: &Statement) -> Result<T, Error> {
        match statement {
            Statement::Expr(e) => self.expression(e),
            Statement::Print(e) => self.print(e),
            Statement::Block(block) => self.block(block),
            Statement::If(predicate, then, otherwise) => self.condition(predicate, then, otherwise.as_ref().map(|x| &**x)),
            Statement::While(predicate, body) => self.while_loop(predicate.as_ref(), body),
            Statement::Return(expr) => self.ret(expr),
	    Statement::Var { name, initialiser } => self.variable(name, initialiser.as_ref()),
            Statement::Function { name, args, body } => self.function(name, &args[..], body),
	    Statement::Class { name, methods, site } => self.class(name, methods, site)
        }
    }

    fn block(&mut self, block: &[Statement]) -> Result<T, Error>;
    fn condition(&mut self, predicate: &Expr, then: &Statement, otherwise: Option<&Statement>) -> Result<T, Error>;
    fn class(&mut self, name: &Token, methods: &[Statement], site: &Token) -> Result<T, Error>;
    fn function(&mut self, name: &Token, args: &[Token], body: &[Statement]) -> Result<T, Error>;
    fn expression(&mut self, expression: &Expr) -> Result<T, Error>;
    fn print(&mut self, expression: &Expr) -> Result<T, Error>;
    fn ret(&mut self, expr: &Expr) -> Result<T, Error>;
    /// Declaration of a variable, with optional initialiser expression 
    fn variable(&mut self, name: &Token, initialiser: Option<&Expr>) -> Result<T, Error>;
    fn while_loop(&mut self, predicate: Option<&Expr>, body: &Statement) -> Result<T, Error>;
}
