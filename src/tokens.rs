use core::fmt;

use crate::Error;

#[derive(Debug, Clone, PartialEq, PartialOrd)]
pub struct Token {
    pub value: TokenValue,
    pub line: usize,
    pub column: usize,
    pub index: usize,
}

#[derive(Clone, Debug, PartialEq, PartialOrd)]
pub enum TokenValue {
    Number(f64),
    String(String),
    Identifier(String),
    Operator(&'static str),
    Whitespace,
    Comment,
    Keyword(&'static str),
    Punctuation(char),
    EOF,
}

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum TokenType {
    Number,
    String,
    Identifier,
    Operator,
    Whitespace,
    Comment,
    Keyword,
    Punctuation,
    EOF,
}

impl Token {
    pub fn get_type(&self) -> TokenType {
        match self.value {
            TokenValue::Number(_) => TokenType::Number,
            TokenValue::String(_) => TokenType::String,
            TokenValue::Identifier(_) => TokenType::Identifier,
            TokenValue::Operator(_) => TokenType::Operator,
            TokenValue::Whitespace => TokenType::Whitespace,
            TokenValue::Comment => TokenType::Comment,
            TokenValue::Keyword(_) => TokenType::Keyword,
            TokenValue::Punctuation(_) => TokenType::Punctuation,
            TokenValue::EOF => TokenType::EOF
        }
    }

    pub fn error_at(&self, message: String, file: String) -> Error {
        Error {
            message,
            file,
            line: self.line,
            column: self.column,
        }
    }

    pub fn name(&self, file: &str) -> Result<&str, Error> {
	match &self.value {
	    TokenValue::Identifier(n) => Ok(n),
	    _ => Err(self.error_at(format!("Expected identifier, found {}", self.get_type()), file.to_owned()))
	}
    }
}

impl fmt::Display for TokenType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match *self {
            TokenType::Number => write!(f, "number"),
            TokenType::String => write!(f, "string"),
            TokenType::Identifier => write!(f, "identifier"),
            TokenType::Operator => write!(f, "operator"),
            TokenType::Whitespace => write!(f, "whitespace"),
            TokenType::Comment => write!(f, "comment"),
            TokenType::Keyword => write!(f, "keyword"),
            TokenType::Punctuation => write!(f, "punctuation"),
            TokenType::EOF => write!(f, "EOF")
        }
    }
}
