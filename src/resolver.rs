use std::{collections::HashMap, mem};

use tokens::Token;

use crate::{Error, interpreter::Interpreter, syntax::{Expr, Program, Statement}, tokens::{self, TokenValue}, visitors::{ExprVisitor, StatementVisitor}};

pub struct Resolver<'a> {
    interpreter: &'a mut Interpreter,
    scopes: Vec<HashMap<String, bool>>,
    current_function: FunctionType
}

#[derive(PartialEq, Eq)]
enum FunctionType {
    None,
    Function
}

impl <'a> Resolver<'a> {
    pub fn new(interpreter: &'a mut Interpreter) -> Resolver<'a> {
	Resolver {
	    interpreter,
	    scopes: vec!(HashMap::new()),
	    current_function: FunctionType::None
	}
    }

    pub fn resolve(&mut self, program: Program) -> Result<Program, Error> {
	for stmt in &program {
	    self.visit_statement(stmt)?;
	}

	Ok(program)
    }

    fn begin(&mut self) {
	self.scopes.push(HashMap::new());
    }

    fn end(&mut self) {
	self.scopes.pop().expect("ICE: Popping the last environment");
    }

    fn declare(&mut self, identifier: &Token) -> Result<(), Error> {
	let name = identifier.name(&self.interpreter.filename)?;
	if let Some(scope) = self.scopes.last_mut() {
	    if scope.contains_key(name) {
		return Err(identifier.error_at(format!("Variable {} is already defined in this scope", name), self.interpreter.filename.to_owned()))
	    }
	    scope.insert(name.to_owned(), false);
	}
	Ok(())
    }

    fn define(&mut self, name: &str) {
	if let Some(scope) = self.scopes.last_mut() {
	    scope.insert(name.to_owned(), true);
	}
    }

    fn local(&mut self, name: &str, token: &Token) {
	for (level, scope) in self.scopes.iter().rev().enumerate() {
	    if scope.contains_key(name) {
		self.interpreter.resolve(token, level)
	    }
	}
    }

    fn resolve_fn(&mut self, args: &[Token], body: &[Statement], fn_type: FunctionType) -> Result<(), Error> {
	let mut enclosing = fn_type;
	mem::swap(&mut enclosing, &mut self.current_function);
	self.begin();
	for arg in args {
	    self.declare(arg)?;
	    self.define(arg.name(&self.interpreter.filename)?);
	}

	for stmt in body {
	    self.visit_statement(stmt)?;
	}

	self.end();
	mem::swap(&mut enclosing, &mut self.current_function);
	Ok(())

    }
}

impl <'a> StatementVisitor<()> for Resolver<'a> {
    fn block(&mut self, block: &[Statement]) -> Result<(), Error> {
        self.begin();
	for stmt in block {
	    self.visit_statement(stmt)?;
	}
	self.end();
	Ok(())
    }

    fn condition(&mut self, predicate: &Expr, then: &Statement, otherwise: Option<&Statement>) -> Result<(), Error> {
	self.visit_expression(predicate)?;
	self.visit_statement(then)?;
	if let Some(otherwise) = otherwise {
	    self.visit_statement(otherwise)?;
	}
	Ok(())
    }

    fn class(&mut self, name: &Token, _methods: &[Statement], _site: &Token) -> Result<(), Error> {
	let class_name = name.name(&self.interpreter.filename)?;
	self.declare(name)?;
	self.define(class_name);
	Ok(())
    }

    fn function(&mut self, name: &Token, args: &[Token], body: &[Statement]) -> Result<(), Error> {
	self.declare(name)?;
	self.define(name.name(&self.interpreter.filename)?);
	self.resolve_fn(args, body, FunctionType::Function)
    }

    fn expression(&mut self, expr: &Expr) -> Result<(), Error> {
        self.visit_expression(expr)
    }

    fn print(&mut self, expr: &Expr) -> Result<(), Error> {
        self.visit_expression(expr)
    }

    fn ret(&mut self, expr: &Expr) -> Result<(), Error> {
	if self.current_function == FunctionType::None {
	    return Err(self.interpreter.error("Cannot return from top-level code".to_owned(), expr.leading_token()))
	}
        self.visit_expression(expr)
    }

    fn variable(&mut self, token: &Token, initialiser: Option<&Expr>) -> Result<(), Error> {
	self.declare(token)?;
	if let Some(init) = initialiser {
	    self.visit_expression(init)?;
	}
        self.define(token.name(&self.interpreter.filename)?);
	Ok(())
    }

    fn while_loop(&mut self, predicate: Option<&Expr>, body: &Statement) -> Result<(), Error> {
	if let Some(p) = predicate {
	    self.visit_expression(p)?;
	}
	self.visit_statement(body)
    }
}

impl <'a> ExprVisitor<()> for Resolver<'a> {
    fn assignment(&mut self, identifier: &Token, value: &Expr) -> Result<(), Error> {
	self.visit_expression(value)?;
	let name = match &identifier.value {
	    TokenValue::Identifier(n) => n,
	    _ => return Err(identifier
			    .error_at(format!("Expected identifier, found {}", identifier.get_type())
				      , self.interpreter.filename.to_owned()))
	};

	self.local(name, identifier);
	Ok(())
    }

    fn binary(&mut self, w: &Expr, _operator: &Token, x: &Expr) -> Result<(), Error> {
	self.visit_expression(w)?;
	self.visit_expression(x)
    }

    fn call(&mut self, function: &Expr, args: &[Expr], _site: &Token) -> Result<(), Error> {
	self.visit_expression(function)?;
	for arg in args {
	    self.visit_expression(arg)?;
	}
	Ok(())
    }

    fn group(&mut self, expr: &Expr, _site: &Token) -> Result<(), Error> {
	self.visit_expression(expr)
    }

    fn variable(&mut self, identifier: &Token) -> Result<(), Error> {
	let name = match &identifier.value {
	    tokens::TokenValue::Identifier(s) => s,
	    _ => panic!("ICE: resolving an identifier that is not a variable")
	};
        if let Some(scope) = self.scopes.last() {
	    if let Some(false) = scope.get(name) {
		return Err(identifier.error_at(format!("Cannot read local variable in its own initialiser"), self.interpreter.filename.to_owned()))
	    }
	}

	self.local(name, identifier);
	
	Ok(())
    }

    fn literal(&mut self, _token: &Token) -> Result<(), Error> {
        Ok(())
    }

    fn unary(&mut self, _operator: &Token, operand: &Expr) -> Result<(), Error> {
        self.visit_expression(operand)
    }

    fn logical(&mut self, w: &Expr, _operator: &Token, x: &Expr) -> Result<(), Error> {
        self.visit_expression(w)?;
	self.visit_expression(x)
    }
}
