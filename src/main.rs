#![feature(toowned_clone_into)]

use std::{env, fs, io, process};
use std::io::{Read, stdout, Write};

use interpreter::Interpreter;
use resolver::Resolver;

mod tokens;
mod lexer;
mod syntax;
mod parser;
mod visitors;
mod interpreter;
mod resolver;

fn main() {
    match &env::args().skip(1).collect::<Vec<_>>()[..] {
        [] => repl(),
        [p] => file(p),
        _ => help()
    }
}

fn repl() {
    let stdin = io::stdin();
    let mut line = String::new();
    let mut interpreter = Interpreter::new("repl");
    loop {
        print!("→ ");
        stdout().flush().expect("Could not flush stdout");
        line.clear();
        stdin.read_line(&mut line).expect("Could not read from stdin");
        run(&line, &mut interpreter);
    }
}

fn file(p: &str) {
    let mut f = fs::File::open(p).expect("Cannot open file.");
    let mut src = String::new();
    f.read_to_string(&mut src).expect("Cannot read file.");
    if !run(&src, &mut Interpreter::new(p)) {
        process::exit(70)
    } else {
        process::exit(0)
    }
}

fn help() {
    println!("RLox\nUsage: rlox [script]");
}

fn run(src: &str, interpreter: &mut interpreter::Interpreter) -> bool {
    match lexer::lex(src, &interpreter.filename)
        .and_then(|tokens| parser::parse(&tokens[..], &interpreter.filename))
        .and_then(|program| { Resolver::new(interpreter).resolve(program) })
        .and_then(|program| interpreter.evaluate(program)) {
        Ok(()) => true,
        Err(Error { message, file, line, column }) => {
            eprintln!("[{} {}:{}] Error: {}", file, line, column, message);
            eprintln!("{}", src.split('\n').skip(line).next().unwrap());
            for _ in 0..column {
                eprint!(" ");
            }
            eprintln!("^");
            false
        }
    }
}

#[derive(Debug)]
pub struct Error {
    // the actual error message
    pub message: String,
    // where in the source file
    pub file: String,
    pub line: usize,
    pub column: usize,
}

#[test]
fn test_shadowed_variables() {
    // This should fail, as shadowed variables are disallowed
    assert!(!run("var a=1; {var a=a+2; print a;}", &mut Interpreter::new("test")))
}

#[test]
fn test_closures() {
    assert!(run("fun makeCounter() { var i=0; fun count() { i=i+1; print i; } return count; } var c = makeCounter(); c(); c(); c();",
    &mut Interpreter::new("test")));
}

#[test]
fn test_function_call() {
    assert!(run("fun add(x,y){return x+y;} print add(2,3);", &mut Interpreter::new("test")))
}
