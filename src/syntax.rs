use crate::tokens::Token;

pub type Program = Vec<Statement>;

#[derive(PartialEq, Debug, Clone)]
pub enum Statement {
    Expr(Expr),
    Print(Expr),
    Block(Vec<Statement>),
    If(Expr, Box<Statement>, Option<Box<Statement>>),
    While(Option<Expr>, Box<Statement>),
    Return(Expr),
    Var { name: Token, initialiser: Option<Expr> },
    Function { name: Token, args: Vec<Token>, body: Vec<Statement> },
    Class { name: Token, methods: Vec<Statement>, site: Token },
}

#[derive(PartialEq, Debug, Clone)]
pub enum Expr {
    Binary {
        left: Box<Expr>,
        operator: Token,
        right: Box<Expr>,
    },
    Logical {
        left: Box<Expr>,
        operator: Token,
        right: Box<Expr>,
    },
    Group { site: Token, expr: Box<Expr> },
    Literal { token: Token },
    Variable { token: Token },
    Unary {
        operator: Token,
        right: Box<Expr>,
    },
    Assignment {
        name: Token,
        value: Box<Expr>,
    },
    Call {
        site: Token,
        function: Box<Expr>,
        args: Vec<Expr>,
    },
}

impl Expr {
    pub(crate) fn leading_token(&self) -> &Token {
        match self {
            Expr::Binary { operator, .. } => operator,
            Expr::Logical { operator, .. } => operator,
            Expr::Group { site, .. } => site,
            Expr::Literal { token } => token,
            Expr::Variable { token } => token,
            Expr::Unary { operator, .. } => operator,
            Expr::Assignment { name, .. } => name,
            Expr::Call { site, .. } => site,
        }
    }
}
