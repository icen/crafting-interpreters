use std::mem;
use std::collections::HashMap;
use std::time::{SystemTime, UNIX_EPOCH};
use crate::Error;
use crate::tokens::{Token, TokenValue};
use crate::syntax::*;
use crate::visitors::*;
use self::value::Value;
use self::environment::Environment;

mod environment;
mod value;

pub struct Interpreter {
    pub filename: String,
    scope: Environment,
    ret_stack: Vec<Value>,
    locals: HashMap<(usize, usize), usize>,
}

impl Interpreter {
    pub fn new(filename: &str) -> Interpreter {
        let mut interpreter = Interpreter {
            filename: filename.to_owned(),
            scope: Environment::new(),
            ret_stack: Vec::new(),
	    locals: HashMap::new()
        };

        interpreter.scope.define("clock".to_owned(),
                                  Value::Native {
                                      name: "clock",
                                      function: || {
                                          let start = SystemTime::now();
                                          let since_epoch = start.duration_since(UNIX_EPOCH)
                                              .expect("time went backwards");
                                          Value::Number(since_epoch.as_secs_f64())
                                      },
                                  });
        interpreter
    }

    fn push(&mut self) {
        self.scope = Environment::push(self.scope.clone());
    }

    fn pop(&mut self) {
        match self.scope.parent() {
            Some(env) => self.scope = env,
            None => panic!("ICE: Attempted to pop global scope")
        }
    }

    pub fn error(&self, message: String, cause: &Token) -> Error {
        cause.error_at(message, self.filename.clone())
    }

    pub fn evaluate(&mut self, program: Program) -> Result<(), Error> {
        for stmt in program {
            self.visit_statement(&stmt)?;
        }
        Ok(())
    }

    pub fn resolve(&mut self, token: &Token, depth: usize) {
	self.locals.insert((token.line, token.column), depth);

    }

    fn lookup_variable(&self, name: &str, identifier: &Token) -> Result<Value, Error> {
	if let Some(depth) = self.locals.get(&(identifier.line, identifier.column)) {
	    self.scope.lookup_at(name, *depth)
		.ok_or(identifier.error_at(format!("Unbound local variable {}", name), self.filename.to_owned()))
	} else {
	    self.scope.lookup(name)
		.ok_or(identifier.error_at(format!("Unbound global variable {}", name), self.filename.to_owned()))
	}
    }
}

impl StatementVisitor<()> for Interpreter {
    fn variable(&mut self, identifier: &Token, initialiser: Option<&Expr>) -> Result<(), Error> {
        let value = if let Some(expr) = initialiser {
            self.visit_expression(expr)?
        } else {
            Value::Nil
        };

	let name = identifier.name(&self.filename)?.to_owned();
	if self.locals.contains_key(&(identifier.line, identifier.column)) {
            self.scope.define(name, value);
	} else {
	    self.scope.define(name, value);
	}
        Ok(())
    }

    fn function(&mut self, identifier: &Token, args: &[Token], body: &[Statement]) -> Result<(), Error> {
	let name = identifier.name(&self.filename)?.to_owned();
	if self.locals.contains_key(&(identifier.line, identifier.column)) {
            self.scope.define(name, Value::Function(value::Function {
		args: args.to_vec(),
		body: body.to_vec(),
		env: self.scope.clone(),
            }));
	} else {
	    self.scope.define(name, Value::Function(value::Function {
		args: args.to_vec(), body: body.to_vec(), env: self.scope.clone()
	    }))
	}
        Ok(())
    }

    fn block(&mut self, block: &[Statement]) -> Result<(), Error> {
        self.push();
        for stmt in block {
            match self.visit_statement(stmt) {
                Ok(()) => (),
                Err(e) => {
                    self.pop();
                    return Err(e);
                }
            }
        }
        self.pop();
        Ok(())
    }

    fn expression(&mut self, expression: &Expr) -> Result<(), Error> {
        self.visit_expression(expression)?;
        Ok(())
    }

    fn print(&mut self, expression: &Expr) -> Result<(), Error> {
        Ok(println!("{}", self.visit_expression(expression)?))
    }

    fn condition(&mut self, predicate: &Expr, then: &Statement, otherwise: Option<&Statement>) -> Result<(), Error> {
        match self.visit_expression(predicate)? {
            Value::Nil | Value::Bool(false) => {
                if let Some(branch) = otherwise {
                    self.visit_statement(branch)
                } else {
                    Ok(())
                }
            }
            Value::Bool(true) | _ => {
                self.visit_statement(then)
            }
        }
    }

    fn while_loop(&mut self, predicate: Option<&Expr>, body: &Statement) -> Result<(), Error> {
        if let Some(predicate) = predicate {
            while self.visit_expression(predicate)?.truthy() {
                self.visit_statement(body)?;
            }
        }

        Ok(())
    }

    fn ret(&mut self, expr: &Expr) -> Result<(), Error> {
        let val = self.visit_expression(expr)?;
        assert_eq!(self.ret_stack.pop(), Some(Value::Nil));
        self.ret_stack.push(val);
        Ok(())
    }

    fn class(&mut self, name: &Token, _methods: &[Statement], _site: &Token) -> Result<(), Error> {
	let class_name = name.name(&self.filename)?;
	self.scope.define(class_name.to_owned(),
			  Value::Class(value::Class { name: class_name.to_owned(), methods: HashMap::new() }));
	Ok(())
    }
}

impl ExprVisitor<Value> for Interpreter {
    fn variable(&mut self, identifier: &Token) -> Result<Value, Error> {
        match &identifier.value {
            TokenValue::Identifier(name) => {
		self.lookup_variable(name, identifier)
	    },
            _ => Err(Error {
                message: format!("ICE: Not a variable: {:?}", identifier.get_type()),
                line: identifier.line,
                column: identifier.column,
                file: self.filename.clone(),
            })
        }
    }

    fn assignment(&mut self, identifier: &Token, value: &Expr) -> Result<Value, Error> {
        let v = self.visit_expression(value)?;
        match &identifier.value {
            TokenValue::Identifier(name) => {
		if let Some(depth) = self.locals.get(&(identifier.line, identifier.column)) {
		    self.scope.amend_at(name, v.clone(), *depth)
		} else {
		    self.scope.amend_at(name, v.clone(), 0);
                }
            }
            _ => return Err(self.error(format!("Expected identifier, found {}", identifier.get_type()), identifier)),
        };

        Ok(v)
    }

    fn literal(&mut self, token: &Token) -> Result<Value, Error> {
        match &token.value {
            TokenValue::Number(number) => Ok(Value::Number(*number)),
            TokenValue::String(string) => Ok(Value::String(string.to_owned())),
            TokenValue::Keyword("true") => Ok(Value::Bool(true)),
            TokenValue::Keyword("false") => Ok(Value::Bool(false)),
            TokenValue::Keyword("nil") => Ok(Value::Nil),
            _ => Err(Error {
                message: format!("ICE: Not a literal: {:?}", token.get_type()),
                line: token.line,
                column: token.column,
                file: self.filename.clone(),
            })
        }
    }

    fn unary(&mut self, operator: &Token, operand: &Expr) -> Result<Value, Error> {
        match (&operator.value, self.visit_expression(operand)?) {
            (TokenValue::Operator("!"), Value::Bool(b)) => Ok(Value::Bool(!b)),
            (TokenValue::Operator("!"), Value::Nil) => Ok(Value::Bool(true)),
            (TokenValue::Operator("!"), _) => Ok(Value::Bool(false)),
            (TokenValue::Operator("-"), Value::Number(n)) => Ok(Value::Number(-n)),

            (TokenValue::Operator("-"), v) => Err(self.error(format!("Type error: expected number, found {}", v.describe()), operator)),
            (TokenValue::Operator(_), Value::Nil) => Err(self.error(format!("Argument is nil"), operator)),

            (t, _) => Err(self.error(format!("ICE: Unexpected unary operator: {:?}", t), operator))
        }
    }

    fn binary(&mut self, w: &Expr, operator: &Token, x: &Expr) -> Result<Value, Error> {
        let left = self.visit_expression(w)?;
        let right = self.visit_expression(x)?;

        match (left, &operator.value, right) {
            (w, TokenValue::Operator("=="), x) => Ok(Value::Bool(w == x)),
            (w, TokenValue::Operator("!="), x) => Ok(Value::Bool(w != x)),
            (Value::Number(w), TokenValue::Operator(op), Value::Number(x)) => match *op {
                "+" => Ok(Value::Number(w + x)),
                "-" => Ok(Value::Number(w - x)),
                "*" => Ok(Value::Number(w * x)),
                "/" => {
                    if x == 0f64 {
                        return Err(self.error(format!("Division by zero"), operator));
                    }
                    Ok(Value::Number(w / x))
                }
                "<" => Ok(Value::Bool(w < x)),
                "<=" => Ok(Value::Bool(w <= x)),
                ">" => Ok(Value::Bool(w > x)),
                ">=" => Ok(Value::Bool(w >= x)),
                op => Err(self.error(format!("ICE: Unexpected binary operator: {}", op), operator))
            },

            (w, TokenValue::Operator("+"), x) => Ok(Value::String(format!("{}{}", w, x))),
            (w, TokenValue::Operator(op), x) => Err(self.error(format!("Binary operator {} not valid between operands of type {} and {}", op, w.describe(), x.describe()), operator)),
            _ => Err(self.error(format!("ICE: Unexpected binary operator: {:?}", operator), operator))
        }
    }

    fn group(&mut self, expr: &Expr, _site: &Token) -> Result<Value, Error> {
        self.visit_expression(expr)
    }

    fn logical(&mut self, w: &Expr, operator: &Token, x: &Expr) -> Result<Value, Error> {
        match operator.value {
            TokenValue::Keyword("and") => {
                let left = self.visit_expression(w)?;
                if left.truthy() {
                    Ok(Value::Bool(self.visit_expression(x)?.truthy()))
                } else {
                    Ok(Value::Bool(false))
                }
            }
            TokenValue::Keyword("or") => {
                let left = self.visit_expression(w)?;
                if !left.truthy() {
                    Ok(Value::Bool(self.visit_expression(x)?.truthy()))
                } else {
                    Ok(Value::Bool(true))
                }
            }
            _ => Err(self.error(format!("ICE: Invalid logical operator {:?}", operator.value), operator))
        }
    }

    fn call(&mut self, function: &Expr, supplied: &[Expr], site: &Token) -> Result<Value, Error> {
        match self.visit_expression(function)? {
            Value::Function(value::Function { args, body, mut env }) => {
                if args.len() != supplied.len() {
                    return Err(site.error_at(
                        format!("Function called with the wrong number of arguments: expected {}, found {}", args.len(), supplied.len()),
                        self.filename.clone(),
                    ));
                }

                let values: Vec<Value> = supplied.iter().map(|expr| self.visit_expression(expr)).collect::<Result<Vec<_>, Error>>()?;
                mem::swap(&mut self.scope, &mut env);
                self.push();
                for (arg, value) in args.iter().zip(values.iter()) {
                    self.scope.define(arg.name(&self.filename)?.to_owned(), value.clone());
                }
                self.ret_stack.push(Value::Nil);
                let result = self.evaluate(body);
                self.pop();
                mem::swap(&mut self.scope, &mut env);

                match result {
                    Ok(()) => match self.ret_stack.pop() {
                        Some(v) => Ok(v),
                        None => Err(site.error_at(format!("ICE: Function call yielded no result"), self.filename.clone()))
                    }
                    Err(e) => Err(e)
                }
            },
            Value::Native { function: native, .. } => {
                if !supplied.is_empty() {
                    return Err(function.leading_token().error_at(
                        format!("Function called with the wrong number of arguments: expected 0, found {}", supplied.len()),
                        self.filename.clone(),
                    ));
                }

                Ok(native())
            }
	    Value::Class(class)  => {
		let mut state = Environment::new();

		for (name, body) in &class.methods {
		    state.define(name.to_owned(), Value::Function(body.clone()))
		}

		Ok(Value::Instance { class, state: Environment::new() })
	    },
            v => Err(function.leading_token().error_at(
                format!("Cannot call {} as function", v.describe()),
                self.filename.clone(),
            ))
        }
    }
}

